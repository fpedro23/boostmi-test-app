import UIKit

// MARK: Top levels routes

enum Route: Equatable {
    case home
    case showMovieDetails(MovieDetailsState)
    case showMovieReview(MovieReview)
}
