import UIKit

protocol AlertPresentable {

    var rootViewController: UIViewController { get }

    func present(_ alert: Alert)
}

// MARK: - AlertPresentable

extension AlertPresentable {
    func present(_ alert: Alert) {
        let viewController = alert.createAlertController()
        rootViewController.topmostViewController().present(viewController, animated: UI.animated, completion: nil)
    }
}
