import UIKit

class Alert {

    // MARK: Properties

    var alertActions: [AlertAction] = []

    let message: String?

    var preferredAction: AlertAction?

    let preferredStyle: UIAlertController.Style
     
    let title: String?

    // MARK: Initialization

    init(title: String?, message: String?, preferredStyle: UIAlertController.Style, alertActions: [AlertAction] = []) {
        self.title = title
        self.message = message
        self.preferredStyle = preferredStyle
        self.alertActions = alertActions
    }

    // MARK: Methods

    func add(_ action: AlertAction) -> Self {
        alertActions.append(action)
        return self
    }

    func addPreferred(_ action: AlertAction) -> Self {
        alertActions.append(action)
        preferredAction = action
        return self
    }

    func createAlertController() -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)

        alertActions.forEach { alertAction in
            let action = UIAlertAction(title: alertAction.title, style: alertAction.style) { _ in
                alertAction.handler?(alertAction)
            }

            alert.addAction(action)

            if let preferredAction = preferredAction, preferredAction === alertAction {
                alert.preferredAction = action
            }
        }

        return alert
    }
}
