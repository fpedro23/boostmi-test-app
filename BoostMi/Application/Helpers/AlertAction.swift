import UIKit

class AlertAction {

    // MARK: Properties

     
    let handler: ((AlertAction) -> Void)?

     
    let style: UIAlertAction.Style

     
    let title: String?

    // MARK: Initialization
    init(title: String?, style: UIAlertAction.Style, handler: ((AlertAction) -> Void)? = nil) {
        self.title = title
        self.style = style
        self.handler = handler
    }
}
