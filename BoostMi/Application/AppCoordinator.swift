import UIKit
import SwiftUI

protocol AppCoordinator: Navigation {

}

class DefaultAppCoordinator: NSObject {

    typealias Services = HasHomeAPIService


    var viewController: UIViewController {
        return navigationController
    }

    let navigationController: UINavigationController = UINavigationController()

    private var homeProcessor: HomeProcessor?

    private var movieDetailsProcessor: MovieDetailsProcessor?

    private var presentedCoordinator: AnyObject?

    private let services: Services

    // MARK: Initialization

    init(services: Services) {
        self.services = services

        super.init()
    }

    private func navigateBack(completion: (() -> Void)? = nil) {
        if presentedCoordinator != nil {
            navigationController.dismiss(animated: UI.animated) {
                if self.navigationController.presentedViewController == nil {
                    self.presentedCoordinator = nil
                }
                completion?()
            }
        } else {
            navigationController.popViewController(animated: UI.animated)
            completion?()
        }
    }
}

// MARK: - AppCoordinator

extension DefaultAppCoordinator: AppCoordinator {
    var rootViewController: UIViewController {
        return viewController
    }

    func navigate(to route: Route, context: AnyObject?) {
        switch route {
        case .home:
            showHome()
        case .showMovieDetails(let movie):
            showMovieDetails(movie, context: context as? MovieDetailsDelegate)
        case .showMovieReview(let review):
            showMovieReview(review: review, context: context as? MovieReviewDelegate)
        }
    }

    func navigateBack(show route: Route?) {
        navigateBack {
            guard let route = route else { return }
            self.navigate(to: route)
        }
    }

    private func showHome() {
        let homeViewController = HomeViewController()
        homeProcessor = HomeProcessor(navigation: self,
                                      presenter: homeViewController,
                                      services: services,
                                      state: HomeState())
        homeViewController.dispatcher = homeProcessor?.dispatcher
        navigationController.pushViewController(homeViewController, animated: false)
    }

    private func showMovieDetails(_ state: MovieDetailsState, context: MovieDetailsDelegate?) {
        let detailsViewController = MovieDetailsViewController()
        movieDetailsProcessor = MovieDetailsProcessor(navigation: self,
                                                      presenter: detailsViewController,
                                                      delegate: context,
                                                      state: state)
        detailsViewController.dispatcher = movieDetailsProcessor?.dispatcher
        navigationController.pushViewController(detailsViewController, animated: true)
    }

    private func showMovieReview(review: MovieReview, context: MovieReviewDelegate?) {
        let hostingController = UIHostingController(rootView:
            MovieReviewView(movieReview: review,
                            delegate: context,
                            navigation: self)
        )

        navigationController.pushViewController(hostingController, animated: true)
    }
}

protocol MovieReviewDelegate: class {
    func didAddMovieReview(review: MovieReview)
}

