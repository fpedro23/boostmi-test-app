//
//  SceneDelegate.swift
//  BoostMi
//
//  Created by Pedro Contreras on 2020-01-13.
//  Copyright © 2020 Pedro Contreras. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    var appCoordinator: AppCoordinator?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        let apiService = Service(baseURL: URL(string: "https://api.themoviedb.org/3")!)
//        let apiService = Service(baseURL: URL(string: "https://local")!, client: TestClient())
        let services = ServiceContainer(apiService: apiService)
        let appCoordinator = DefaultAppCoordinator(services: services)
        self.appCoordinator = appCoordinator
        let window = UIWindow(windowScene: scene)
        window.rootViewController = appCoordinator.viewController
        window.makeKeyAndVisible()
        self.window = window
        appCoordinator.navigate(to: .home)
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

