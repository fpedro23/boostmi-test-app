import Foundation

protocol HasAPIService {
    var apiService: APIService { get }
}

protocol HasHomeAPIService {

     
    var homeAPIService: HomeAPIService { get }
}

private typealias Services = HasAPIService

struct ServiceContainer: Services {

    // MARK: Properties

    let apiService: APIService

    init(apiService: APIService) {
        self.apiService = apiService
    }
}

// MARK: - HasHomeAPIService

extension ServiceContainer: HasHomeAPIService {
    var homeAPIService: HomeAPIService {
        apiService
    }
}
