import Foundation


final class Dispatcher<T> {

    private let receive: (T) -> Void

    init<P: Processor>(_ processor: P) where P.ActionType == T {
        self.receive = { [weak processor] action in
            processor?.receive(action)
        }
    }

    init<A>(via dispatcher: Dispatcher<A>?, map: @escaping (T) -> A) {
        self.receive = { [weak dispatcher] action in
            dispatcher?.receive(map(action))
        }
    }

    func dispatch(_ action: T) {
        receive(action)
    }
}

protocol Processor: class {
    associatedtype ActionType

    func receive(_ action: ActionType)
}
