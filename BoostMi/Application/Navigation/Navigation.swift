import Foundation

 
 
protocol Navigation: AlertPresentable, Dismisser {
    func navigate(to route: Route, context: AnyObject?)

    func navigate(to route: Route)
}

extension Navigation {
    func navigate(to route: Route) {
        navigate(to: route, context: nil)
    }
}

 
protocol Dismisser: class {
     
    func navigateBack(show route: Route?)
}

extension Dismisser {
     
    func navigateBack() {
        navigateBack(show: nil)
    }
}
