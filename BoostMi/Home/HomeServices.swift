import Foundation

typealias HomeServices = HomeAPIService

protocol HomeAPIService {

    func getMovies(page: Int) -> Task<IMBDResponse<[Movie]>, Error>

    func searchMovies(query: String) -> Task<IMBDResponse<[Movie]>, Error>
}

extension Service: HomeAPIService {
    func searchMovies(query: String) -> Task<IMBDResponse<[Movie]>, Error> {
        send(MovieSearchRequest(searchQuery: query))
    }

    func getMovies(page: Int) -> Task<IMBDResponse<[Movie]>, Error> {
        send(MovieListRequest(page: page))
    }
}
