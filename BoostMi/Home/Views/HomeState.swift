import Foundation
import UIKit

struct HomeState: Equatable {

    enum Section {
        case main
    }

    enum DisplayedInformation {
        case discover
        case favourites
        case search
    }

    var movies: [Movie] = []

    var favouriteMovies: Set<Movie> = []

    var displayedInformation: DisplayedInformation = .discover

    var isLoading: Bool = false

    var currentPage = 1

    mutating func favouriteDidChange(for movie: MovieDetailsState) {
        guard let firstMatch = movies.first(where: { $0.id == movie.identifier }) else {
             return
        }
        if movie.isFavourite {
            favouriteMovies.insert(firstMatch)
        } else {
            favouriteMovies.remove(firstMatch)
        }
    }

}

extension HomeState {
    var collectionViewState: NSDiffableDataSourceSnapshot<Section, MovieViewState> {
        var snapshot = NSDiffableDataSourceSnapshot<Section, MovieViewState>()
        snapshot.appendSections([.main])
        switch displayedInformation {
        case .discover, .search:
            let set = NSOrderedSet(array: movies)
            snapshot.appendItems(set.compactMap{ MovieViewState(movie: $0 as! Movie, isLoading: isLoading)})
        case .favourites:
            snapshot.appendItems(favouriteMovies.compactMap{ MovieViewState(movie: $0, isLoading: isLoading)})
        }
        return snapshot
    }
}

extension HomeState {
    var favouriteImage: UIImage {
        displayedInformation == .favourites ? UIImage(systemName: "heart.fill")! : UIImage(systemName: "heart")!
    }
}
