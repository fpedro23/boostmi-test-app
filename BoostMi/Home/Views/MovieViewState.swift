import Foundation

struct MovieViewState: Hashable {

    let identifier: Int

    let overview: String

    let posterPath: URL?

    let title: String

    let isLoading: Bool
}

extension MovieViewState {
    init?(movie: Movie, isLoading: Bool) {
        guard let identifier = movie.id else { return nil }
        self.identifier = identifier
        self.overview = movie.overview ?? ""
        if let path = movie.posterPath {
            let baseURL = "https://image.tmdb.org/t/p/w780\(path)"
            if let url = URL(string: baseURL) {
                self.posterPath = url
            } else {
                self.posterPath = nil
            }
        } else {
            self.posterPath = nil
        }

        self.title = movie.title ?? ""
        self.isLoading = isLoading
    }
}
