import UIKit

class MovieViewCell: UICollectionViewCell {

    static let reusableIdentifier = String(describing: MovieViewCell.self)

    let imageView = UIImageView()

    let titleLabel = UILabel()

    lazy var stackView = UIStackView(arrangedSubviews: [imageView, titleLabel], axis: .vertical)

    override init(frame: CGRect) {
        super.init(frame: frame)
        addConstrained(subview: imageView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }

    func present(_ state: MovieViewState) {
        self.titleLabel.text = state.title
        guard let url = state.posterPath else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("Failed fetching image:", error)
                return
            }

            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                print("Not a proper HTTPURLResponse or statusCode")
                return
            }

            DispatchQueue.main.async {
                self.imageView.image = UIImage(data: data!)
            }
        }.resume()
    }
}
