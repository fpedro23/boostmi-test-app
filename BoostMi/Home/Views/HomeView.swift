import UIKit

class HomeView: UIView {

    var dispatcher: Dispatcher<HomeAction>?

    let threshold: CGFloat = 100.0 // threshold from bottom of collectionview

    lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: MoviesCollectionViewLayout.layout)
        collectionView.register(MovieViewCell.self, forCellWithReuseIdentifier: MovieViewCell.reusableIdentifier)
        collectionView.delegate = self
        collectionView.backgroundColor = .systemBackground
        return collectionView
    }()

    lazy var dataSource: UICollectionViewDiffableDataSource<HomeState.Section, MovieViewState> = {
        let dataSource = UICollectionViewDiffableDataSource<HomeState.Section, MovieViewState>(collectionView: collectionView, cellProvider: cellProvider)
        return dataSource
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemBackground
        addConstrained(subview: collectionView)
    }

    func cellProvider(collectionView: UICollectionView, indexPath: IndexPath, movie: MovieViewState) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieViewCell.reusableIdentifier, for: indexPath) as? MovieViewCell
        cell?.present(movie)
        return cell
    }


    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func present(_ state: HomeState) {
        dataSource.apply(state.collectionViewState)
    }
}

extension HomeView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let movie = dataSource.itemIdentifier(for: indexPath) else { return }
        dispatcher?.dispatch(.movieDetails(movie))
    }
}

extension HomeView: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if  maximumOffset - contentOffset <= threshold {
            dispatcher?.dispatch(.loadMovies)
        }
    }
}
