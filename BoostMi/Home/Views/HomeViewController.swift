import UIKit

enum HomeAction: Equatable {
    case loadMovies

    case movieDetails(MovieViewState)

    case search(String)

    case didTapFavourite
}

class HomeViewController: UIViewController {

    var dispatcher: Dispatcher<HomeAction>? {
        didSet {
            homeView.dispatcher = dispatcher
        }
    }

    lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Movies"
        return searchController
    }()

    let homeView = HomeView()

    lazy var favouriteButton: UIBarButtonItem = {
        UIBarButtonItem(image: UIImage(systemName: "heart"),
                        style: .plain,
                        target: self,
                        action: #selector(didTapFavourite))
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.rightBarButtonItem = favouriteButton
        navigationItem.searchController = searchController
        dispatcher?.dispatch(.loadMovies)
        view.addConstrained(subview: homeView)
    }

    @objc func didTapFavourite() {
        dispatcher?.dispatch(.didTapFavourite)
    }
}

extension HomeViewController: HomePresenter {
    func present(_ state: HomeState) {
        title = state.displayedInformation == .favourites ? "Favourites" : "Discover movies"
        favouriteButton.image = state.favouriteImage
        homeView.present(state)
    }
}

extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, !text.isEmpty else {
            dispatcher?.dispatch(.loadMovies)
            return
        }
        dispatcher?.dispatch(.search(text))
    }
}

extension HomeViewController: UISearchControllerDelegate {
    func didDismissSearchController(_ searchController: UISearchController) {
        dispatcher?.dispatch(.loadMovies)
    }
}
