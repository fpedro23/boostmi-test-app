import UIKit

struct MoviesCollectionViewLayout {
    static var layout: UICollectionViewLayout {
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                               heightDimension: .fractionalWidth(3.0))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize,
                                         subitems: [createMainWithTwoLeadingGroup(),
                                                    createTripletGroup(),
                                                    createMainWithTwoTrailingGroup(),
                                                    createTripletGroup(),
                                                    ])
        let section = NSCollectionLayoutSection(group: group)
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }

    static func createMainWithTwoTrailingGroup() -> NSCollectionLayoutGroup {

        let mainItem = NSCollectionLayoutItem(
          layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(2/3),
            heightDimension: .fractionalWidth(1.0)))

        mainItem.contentInsets = NSDirectionalEdgeInsets(
          top: 2,
          leading: 2,
          bottom: 2,
          trailing: 2)

        let pairItemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                   heightDimension: .fractionalWidth(1))

        let pairItem = NSCollectionLayoutItem(layoutSize: pairItemSize)
        pairItem.contentInsets = NSDirectionalEdgeInsets(top: 2,
                                                          leading: 2,
                                                          bottom: 2,
                                                          trailing: 2)

        pairItem.contentInsets = NSDirectionalEdgeInsets(
          top: 2,
          leading: 2,
          bottom: 2,
          trailing: 2)

        let trailingGroup = NSCollectionLayoutGroup.vertical(
          layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1/3),
            heightDimension: .fractionalWidth(1.0)),
          subitem: pairItem,
          count: 2)

        let mainWithPairGroup = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalWidth(1.0)),
          subitems: [trailingGroup,
                    mainItem
        ])

        return mainWithPairGroup
    }

    static func createMainWithTwoLeadingGroup() -> NSCollectionLayoutGroup {

        let mainItem = NSCollectionLayoutItem(
          layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(2/3),
            heightDimension: .fractionalWidth(1.0)))

        mainItem.contentInsets = NSDirectionalEdgeInsets(
          top: 2,
          leading: 2,
          bottom: 2,
          trailing: 2)

        let pairItemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                   heightDimension: .fractionalWidth(1))

        let pairItem = NSCollectionLayoutItem(layoutSize: pairItemSize)
        pairItem.contentInsets = NSDirectionalEdgeInsets(top: 2,
                                                          leading: 2,
                                                          bottom: 2,
                                                          trailing: 2)

        pairItem.contentInsets = NSDirectionalEdgeInsets(
          top: 2,
          leading: 2,
          bottom: 2,
          trailing: 2)

        let trailingGroup = NSCollectionLayoutGroup.vertical(
          layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1/3),
            heightDimension: .fractionalWidth(1.0)),
          subitem: pairItem,
          count: 2)

        let mainWithPairGroup = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalWidth(1.0)),
          subitems: [mainItem,
                     trailingGroup
        ])

        return mainWithPairGroup
    }


    static func createTripletGroup() -> NSCollectionLayoutGroup {
        let movieItemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1/3),
                                                   heightDimension: .fractionalHeight(1))

        let movieItem = NSCollectionLayoutItem(layoutSize: movieItemSize)
        movieItem.contentInsets = NSDirectionalEdgeInsets(top: 2,
                                                          leading: 2,
                                                          bottom: 2,
                                                          trailing: 2)

        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                               heightDimension: .fractionalWidth(0.5))

        let triplet = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                         subitems: [movieItem])

        return triplet
    }
}
