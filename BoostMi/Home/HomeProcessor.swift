import Foundation

protocol HomePresenter: class {
    func present(_ state: HomeState)
}

class HomeProcessor {

    typealias Services = HasHomeAPIService

    private weak var presenter: HomePresenter?

    weak var navigation: Navigation?

    var dispatcher: Dispatcher<HomeAction>?

    var state: HomeState {
        didSet {
            guard state != oldValue else { return }
            presenter?.present(state)
        }
    }

    let services: Services

    init(navigation: Navigation,
         presenter: HomePresenter,
         services: Services,
         state: HomeState) {
        self.navigation = navigation
        self.presenter = presenter
        self.state = state
        self.services = services
        self.dispatcher = Dispatcher(self)
    }
}


extension HomeProcessor: Processor {
    func receive(_ action: HomeAction) {
        switch action {
        case .loadMovies:
            self.state.displayedInformation = .discover
            services.homeAPIService.getMovies(page: state.currentPage).perform { result in
                switch result {
                case .success(let movies):
                    self.state.movies.append(contentsOf: movies.results) 
                    self.state.currentPage += 1
                case .failure(let error):
                    let alert = Alert(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert,
                                      alertActions: [.init(title: "OK", style: .default)])
                    self.navigation?.present(alert)
                }
            }
        case .search(let string):
            self.state.displayedInformation = .search
            services.homeAPIService.searchMovies(query: string).perform { result in
                switch result {
                case .success(let movies):
                    self.state.movies = movies.results
                case .failure(let error):
                    let alert = Alert(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert,
                                      alertActions: [.init(title: "OK", style: .default)])
                    self.navigation?.present(alert)
                }
            }
        case .movieDetails(let state):
            let allMovies = self.state.movies + self.state.favouriteMovies
            guard let movie = allMovies.first(where: { $0.id == state.identifier }) else {
                return
            }
            guard let state = MovieDetailsState(movie: movie, isFavourite: self.state.favouriteMovies.contains(movie)) else { return }
            navigation?.navigate(to: .showMovieDetails(state), context: self)
        case .didTapFavourite:
            if state.displayedInformation == .favourites {
                state.displayedInformation = .discover
                return
            }
            state.displayedInformation = .favourites
        }
    }
}

extension HomeProcessor: MovieDetailsDelegate {
    func favouriteDidChange(for movie: MovieDetailsState) {
        state.favouriteDidChange(for: movie)
    }
}
