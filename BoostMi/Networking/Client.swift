import Foundation

// MARK: - Client

 
public protocol Client {
     
    
     
    
    
    
     
    func send(_ request: HTTPRequest,
              completion: @escaping (_ result: Result<HTTPResponse, Error>) -> Void) -> Cancellable

     
    func cancelAllRequests()
}

// MARK: - ClientRequestHandler

 
public class ClientRequestHandler: RequestHandler {

    // MARK: Properties

     
    private let client: Client

    // MARK: Initialization

     
    
     
    public init(client: Client) {
        self.client = client
    }

    // MARK: RequestHandler

    public func handle(_ request: HTTPRequest) -> Task<HTTPResponse, Error> {
        return Task { completion in
            print(request)
            _ = self.client.send(request) { result in
                completion(result)
            }
        }
    }
}

public protocol Cancellable {
    func cancel()
}
