import Foundation
import Dispatch

public struct Task<Success, Failure: Error> {

    public static func fail(_ error: Failure) -> Task<Success, Failure> {
        return Task<Success, Failure> { $0(.failure(error)) }
    }

    public static func succeed(_ result: Success) -> Task<Success, Failure> {
        return Task<Success, Failure> { $0(.success(result)) }
    }

     
    public static var succeed: Task<Void, Failure> {
        return Task<Void, Failure> { $0(.success(())) }
    }

     
    private let execute: (@escaping (Result<Success, Failure>) -> Void) -> Void

    public init(execute: @escaping (_ completion: @escaping (_ result: Result<Success, Failure>) -> Void) -> Void) {
        self.execute = execute
    }

    public func perform(_ completion: @escaping (_ result: Result<Success, Failure>) -> Void) {
        perform(on: DispatchQueue.main, completion: completion)
    }

    public func perform(on queue: DispatchQueue, completion: @escaping (_ result: Result<Success, Failure>) -> Void) {
        execute { result in
            queue.async {
                completion(result)
            }
        }
    }

    public func map<NewSuccess>(_ transform: @escaping (Success) -> NewSuccess) -> Task<NewSuccess, Failure> {
        return Task<NewSuccess, Failure> { completion in
            self.execute { result in
                completion(result.map(transform))
            }
        }
    }

    public func map<NewSuccess>(_ transform: @escaping (Success) throws -> NewSuccess) -> Task<NewSuccess, Failure> {
        return Task<NewSuccess, Failure> { completion in
            self.execute { result in
                switch result {
                case .success(let value):
                    do {
                        let transformed = try transform(value)
                        completion(.success(transformed))
                    } catch let error as Failure {
                        completion(.failure(error))
                    } catch {
                        fatalError("Error type mismatch")
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }

    public func success(on queue: DispatchQueue = DispatchQueue.main, success: @escaping (Success) -> Void) -> Task<Success, Failure> {
        return Task<Success, Failure> { completion in
            self.execute { result in
                if case let .success(value) = result {
                    queue.async {
                        success(value)
                    }
                }
                completion(result)
            }
        }
    }

    public func failure(on queue: DispatchQueue = DispatchQueue.main, failure: @escaping (Failure) -> Void) -> Task<Success, Failure> {
        return Task<Success, Failure> { completion in
            self.execute { result in
                if case let .failure(error) = result {
                    queue.async {
                        failure(error)
                    }
                }
                completion(result)
            }
        }
    }
}
