import Foundation

// MARK: - Service

public class Service {

    // MARK: Properties

     
    private let baseURL: URL

     
    private let requestHandler: RequestHandler

    // MARK: Initialization

     
    
     
    
    
    public init(baseURL: URL,
                requestHandler: RequestHandler) {
        self.baseURL = baseURL
        self.requestHandler = requestHandler
    }

    public convenience init(baseURL: URL,
                            client: Client = URLSession(configuration: .default)) {
        self.init(baseURL: baseURL,
                  requestHandler: ClientRequestHandler(client: client))
    }

    // MARK: Public
    public func send<R: Request>(_ request: R) -> Task<R.Response, Error> where R.Response: Response {
        return send(request).map { httpResponse -> R.Response in
            try .init(response: httpResponse)
        }
    }

    public func send<R: Request>(_ request: R) -> Task<HTTPResponse, Error> {
        return requestHandler.handle(request.resolve(relativeTo: baseURL))
    }
}

