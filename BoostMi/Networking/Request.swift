import Foundation

public protocol Request {
    associatedtype Response

    var method: HTTPMethod { get }
    var body: Data? { get }
    var path: String { get }
    var headers: [String: String] { get }
    var query: [URLQueryItem] { get }

    func validate(_ response: HTTPResponse) throws -> Result<HTTPResponse, Error>
}

public extension Request {
    var method: HTTPMethod {
        return .get
    }

    var body: Data? {
        return nil
    }

    var headers: [String: String] {
        return [:]
    }

    var query: [URLQueryItem] {
        return []
    }

    func resolve(relativeTo baseURL: URL) -> HTTPRequest {
        var urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: false)!
        urlComponents.queryItems = query.count > 0 ? query : nil

        if urlComponents.path.hasSuffix("/") {
            urlComponents.path.removeLast()
        }

        guard let url = urlComponents.url?.appendingPathComponent(path) else {
            fatalError("🛑 Request `resolve` failed: reason unknown.")
        }


        return HTTPRequest(url: url, method: method, headers: headers, body: body)
    }

    func validate(_ response: HTTPResponse) throws -> Result<HTTPResponse, Error> {
        return .success(response)
    }
}
