import Foundation

public protocol HTTPResponseCompatible {
    init(response: HTTPResponse) throws
}

public struct HTTPResponse: Equatable {
    public let url: URL
    public let statusCode: Int
    public let headers: [String: String]
    public let body: Data

    public init(url: URL, statusCode: Int, headers: [String: String], body: Data) {
        self.url = url
        self.statusCode = statusCode
        self.headers = headers
        self.body = body
    }

    init(data: Data?, response: URLResponse?, error: Error?) throws {
        if let error = error {
            let reason: URLSessionClientError
            if (error as NSError).code == NSURLErrorCancelled {
                reason = .cancelled
            } else {
                reason = .urlSessionError(error)
            }

            throw reason
        }

        guard let data = data else {
            throw URLSessionClientError.noData
        }

        guard let response = response else {
            throw URLSessionClientError.noResponse
        }

        guard let httpURLResponse = response as? HTTPURLResponse else {
            throw URLSessionClientError.invalidResponse(response)
        }

        guard let url = response.url else {
            throw URLSessionClientError.noURL
        }

        let headers = (httpURLResponse.allHeaderFields as? [String: String]) ?? [:]

        self.init(url: url, statusCode: httpURLResponse.statusCode, headers: headers, body: data)
    }
}

import Foundation

public enum URLSessionClientError: Error {
    case cancelled
    case invalidResponse(URLResponse)
    case noData
    case noMethod
    case noRequest
    case noResponse
    case noURL
    case urlSessionError(Error)
}

// MARK: - CustomStringConvertible
extension URLSessionClientError: CustomStringConvertible {

     
    public var description: String {
        switch self {
        case .cancelled:
            return "Canceled"
        case .invalidResponse:
            return "Invalid Response"
        case .noData:
            return "No Data"
        case .noMethod:
            return "No Method"
        case .noRequest:
            return "No Request"
        case .noResponse:
            return "No Response"
        case .noURL:
            return "No URL"
        case .urlSessionError(let error):
            return error.localizedDescription
        }
    }
}
