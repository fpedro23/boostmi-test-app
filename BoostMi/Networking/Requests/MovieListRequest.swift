import Foundation

struct MovieListRequest: Request {

    var path: String = "/discover/movie"

    var page: Int

    typealias Response = IMBDResponse<[Movie]>

    var query: [URLQueryItem] {
        [URLQueryItem(name: "api_key", value: API.key),
         URLQueryItem(name: "page", value: String(page)),
        ]
    }
}
