import Foundation

struct MovieSearchRequest: Request {

    let path: String = "/search/movie"

    let searchQuery: String

    typealias Response = IMBDResponse<[Movie]>

    var query: [URLQueryItem] {
        [URLQueryItem(name: "api_key", value: API.key),
         URLQueryItem(name: "query", value: searchQuery),]
    }
}
