import Foundation

struct Movie: JSONResponse, Equatable, Hashable {
    let id: Int?
    let overview: String?
    let posterPath: String?
    let backdropPath: String?
    let title: String?

    enum CodingKeys: String, CodingKey {
        case id
        case backdropPath = "backdrop_path"
        case overview
        case posterPath = "poster_path"
        case title
    }
}
