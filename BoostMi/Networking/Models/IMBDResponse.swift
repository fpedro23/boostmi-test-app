import Foundation

struct IMBDResponse<T: Codable>: JSONResponse {
    let page: Int

    let totalPages: Int

    let totalResults: Int

    let results: T

    enum CodingKeys: String, CodingKey {
        case page
        case totalPages = "total_pages"
        case totalResults = "total_results"
        case results
    }

}
