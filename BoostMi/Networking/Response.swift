import Foundation

public protocol Response {
    init(response: HTTPResponse) throws
}

public protocol JSONResponse: Response, Codable {
    static var decoder: JSONDecoder { get }
}

public extension JSONResponse {
    init(response: HTTPResponse) throws {
        self = try Self.decoder.decode(Self.self, from: response.body)
    }
}

extension Array: Response where Element: JSONResponse {}
extension Array: JSONResponse where Element: JSONResponse {
    public static var decoder: JSONDecoder {
        return Element.decoder
    }
}
