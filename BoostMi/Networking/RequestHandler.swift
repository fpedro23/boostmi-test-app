import Foundation

// MARK: - RequestHandler

public protocol RequestHandler {
    func handle(_ request: HTTPRequest) -> Task<HTTPResponse, Error>
}
