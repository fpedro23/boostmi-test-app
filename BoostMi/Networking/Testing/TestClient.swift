import Foundation

 
class TestClient: Client {
     
    var simulateLatency: Bool {
        return NSClassFromString("XCTest") == nil ? true : false
    }

    func send(_ request: HTTPRequest, completion: @escaping (Result<HTTPResponse, Error>) -> Void) -> Cancellable {
        let parseResponse = {
            do {
                try completion(.success(self.response(for: request)))
            } catch {
                completion(.failure(error))
            }
        }

        if simulateLatency {
            // All results are delayed a second to simulate a network connection
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                parseResponse()
            }
        } else {
            parseResponse()
        }

        return TestCancellable()
    }

    func cancelAllRequests() {}

    private func response(for request: HTTPRequest) throws -> HTTPResponse {
        let data = try testData(for: request)

        return HTTPResponse(url: request.url, statusCode: 200, headers: [:], body: data)
    }

    private func testData(for request: HTTPRequest) throws -> Data {
        var fileName: String?
        for testData in APITestData.allCases where testData.methods.contains(request.method) {
            if request.url.absoluteString.range(of: testData.regEx, options: .regularExpression) != nil {
                fileName = testData.jsonFileName
                break
            }
        }

        guard
            let resourceName = fileName,
            let url = Bundle.main.url(forResource: resourceName, withExtension: "json") else {
                throw TestError.testDataNotFound
        }

        return try Data(contentsOf: url)
    }
}

class TestCancellable: Cancellable {
    func cancel() {}
}

enum TestError: Error {
    case noResult
    case testDataNotFound
}
