import Foundation

enum APITestData: String, CaseIterable {

    case movieList
    case movieSearch

    var jsonFileName: String {
        rawValue
    }

    var methods: [HTTPMethod] {
        [.delete, .get, .patch, .post, .put]
    }

    var regEx: String {
        switch self {
        case .movieList:
            return ".*/discover/movie.*"
        case .movieSearch:
            return ".*/search/movie.*"
        }
    }
}
