import Foundation

extension URLSession: Client {
    public func send(_ request: HTTPRequest, completion: @escaping (Result<HTTPResponse, Error>) -> Void) -> Cancellable {
        var urlRequest = URLRequest(url: request.url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.httpBody = request.body

        for (field, value) in request.headers {
            urlRequest.addValue(value, forHTTPHeaderField: field)
        }

        let task = dataTask(with: urlRequest) { data, response, error in
            do {
                let httpResponse = try HTTPResponse(data: data,
                                                    response: response,
                                                    error: error)
                completion(.success(httpResponse))
            } catch {
                completion(.failure(error))
            }
        }

        task.resume()
        return task
    }

    public func cancelAllRequests() {
        getAllTasks { tasks in
            for task in tasks {
                task.cancel()
            }
        }
    }
}

extension URLSessionTask: Cancellable {}
