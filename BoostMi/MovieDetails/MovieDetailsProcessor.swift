import UIKit

protocol MovieDetailsPresenter: class {
    func present(_ state: MovieDetailsState)
}

class MovieDetailsProcessor {

    private weak var presenter: MovieDetailsPresenter?

    weak var navigation: Navigation?

    var dispatcher: Dispatcher<MovieDetailsAction>?

    weak var delegate: MovieDetailsDelegate?

    var state: MovieDetailsState {
        didSet {
            guard state != oldValue else { return }
            presenter?.present(state)
        }
    }

    init(navigation: Navigation,
         presenter: MovieDetailsPresenter,
         delegate: MovieDetailsDelegate?,
         state: MovieDetailsState) {
        self.navigation = navigation
        self.presenter = presenter
        self.state = state
        self.delegate = delegate
        self.dispatcher = Dispatcher(self)
    }

}

extension MovieDetailsProcessor: Processor {
    func receive(_ action: MovieDetailsAction) {
        switch action {
        case .loadMovieDetails:
            presenter?.present(state)
        case .didTapFavourite:
            state.isFavourite = !state.isFavourite
            delegate?.favouriteDidChange(for: state)
        case .didTapAddReview:
            let review = MovieReview()
            navigation?.navigate(to: .showMovieReview(review), context: self)
        }
    }
}

extension MovieDetailsProcessor: MovieReviewDelegate {
    func didAddMovieReview(review: MovieReview) {
        state.reviews.append(review)
    }
}
