import UIKit
import SwiftUI

enum MovieDetailsAction {
    case loadMovieDetails
    case didTapFavourite
    case didTapAddReview
}

class MovieDetailsViewController: UIViewController {

    var dispatcher: Dispatcher<MovieDetailsAction>?

    let movieDetailsView = MovieDetailsView()

    lazy var reviewList: UIHostingController<MovieReviewList> = {
        let list = MovieReviewList(reviews: [])
        let controller = UIHostingController(rootView: list)
        return controller
    }()

    lazy var favouriteButton: UIBarButtonItem = {
        UIBarButtonItem(image: UIImage(systemName: "heart"),
                        style: .plain,
                        target: self,
                        action: #selector(didTapImage))
    }()

    lazy var addReviewButton: UIBarButtonItem = {
        UIBarButtonItem(image: UIImage(systemName: "pencil"),
                        style: .plain,
                        target: self,
                        action: #selector(didTapAddReview))
    }()

    lazy var stackView = UIStackView(arrangedSubviews: [movieDetailsView, reviewList.view], axis: .vertical)

    override func viewDidLoad() {
        addChild(reviewList)
        reviewList.didMove(toParent: self)
        view.backgroundColor = .systemBackground
        navigationItem.rightBarButtonItems = [favouriteButton, addReviewButton]
        view.addConstrained(subview: stackView, top: nil)
        movieDetailsView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        dispatcher?.dispatch(.loadMovieDetails)
    }

    @objc func didTapImage() {
        dispatcher?.dispatch(.didTapFavourite)
    }

    @objc func didTapAddReview() {
        dispatcher?.dispatch(.didTapAddReview)
    }
}

extension MovieDetailsViewController: MovieDetailsPresenter {
    func present(_ state: MovieDetailsState) {
        movieDetailsView.present(state)
        self.reviewList.rootView = MovieReviewList(reviews: state.reviews)
        navigationItem.rightBarButtonItem?.image = state.favouriteImage
    }
}
