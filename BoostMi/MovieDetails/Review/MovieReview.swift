import SwiftUI

struct MovieReview: Equatable, Identifiable {
    var id: String {
        reviewText
    }

    var image: Image? = Image("")
    var rating: Double = 1
    var reviewText: String = ""
}
