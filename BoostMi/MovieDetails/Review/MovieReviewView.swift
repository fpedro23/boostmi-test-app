import SwiftUI
import Combine
struct MovieReviewView: View {

    @State var movieReview: MovieReview
    var delegate: MovieReviewDelegate?
    var navigation: Dismisser?

    @State var showImagePicker:Bool = false
    @State var showActionSheet:Bool = false
    @State var sourceType:Int = 0

    var body: some View {
        ZStack {
            VStack {
                movieReview.image?
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(height: 200, alignment: .center)
                    .clipped()
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .overlay(
                        CameraButtonView(showActionSheet: $showActionSheet)
                            .frame(width: 60, height: 60), alignment: .bottomTrailing
                )
                StarSlider(value: $movieReview.rating)
                Text("Leave your review!")
                TextField("Enter your comments", text: $movieReview.reviewText)
                Button("Add Review") {
                    self.delegate?.didAddMovieReview(review: self.movieReview)
                    self.navigation?.navigateBack()
                }
                Spacer()
                
                }.padding()
            .actionSheet(isPresented: $showActionSheet, content: { () -> ActionSheet in
                ActionSheet(title: Text("Select Image"), message: Text("Please select an image from the image gallery or use the camera"), buttons: [
                    ActionSheet.Button.default(Text("Camera"), action: {
                        self.sourceType = 0
                        self.showImagePicker.toggle()
                    }),
                    ActionSheet.Button.default(Text("Photo Gallery"), action: {
                        self.sourceType = 1
                        self.showImagePicker.toggle()
                    }),
                    ActionSheet.Button.cancel()
                ])
            })
            if showImagePicker {
                ImagePicker(isVisibile: $showImagePicker, image: $movieReview.image, sourceType: sourceType)
            }
        }
        .navigationBarTitle(Text("Review"), displayMode: .inline)
    }
}

struct UserReview_Previews: PreviewProvider {
    static var previews: some View {
        let review = MovieReview(image: Image("sanfrancisco"))
        return MovieReviewView(movieReview: review, delegate: nil)
    }
}

struct StarSlider: View {

    @Binding var value: Double

    var body: some View {
        VStack {
            Text("\(Int(value * 4 + 1)) stars")
            HStack {
                Text("0")
                Slider(value: $value)
                    .cornerRadius(10)
                Text("5")
            }
        }
    }
}
