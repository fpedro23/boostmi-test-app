import SwiftUI

struct MovieReviewList: View {
    var reviews: [MovieReview]
    var body: some View {
        VStack(alignment: .leading) {
            Text("Reviews")
                .font(.title)
            List(reviews) { review in
                MovieReviewCell(review: review)
            }
        }.padding()
    }
}

struct MovieReviewCell: View {

    let review: MovieReview

    var body: some View {
        HStack {
            review.image?
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 80, height: 80)
            .clipped()
            VStack(alignment: .leading) {
                Text(review.reviewText)
                Text("Rating: \(Int(review.rating * 4 + 1)) stars")
            }
            Spacer()
        }
    }
}
struct MovieReviewList_Previews: PreviewProvider {
    static var previews: some View {
        MovieReviewList(reviews: [
            MovieReview(image: Image("sanfrancisco"),
                        rating: 0.5,
                        reviewText: "wowow"),
            MovieReview(image: Image("sanfrancisco"),
                        rating: 0.5,
                        reviewText: "nooo")
        ])
    }
}
