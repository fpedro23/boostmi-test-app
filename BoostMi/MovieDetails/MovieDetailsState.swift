import UIKit
import Foundation

struct MovieDetailsState: Equatable {

    let identifier: Int

    let title: String

    let overview: String

    var isFavourite: Bool = false

    var backdropPath: URL?

    var reviews: [MovieReview] = []
}

extension MovieDetailsState{
    init?(movie: Movie, isFavourite: Bool) {
        guard let identifier = movie.id else { return nil }
        self.identifier = identifier
        self.isFavourite = isFavourite
        self.title = movie.title ?? ""
        self.overview = movie.overview ?? ""
        if let path = movie.backdropPath {
            let baseURL = "https://image.tmdb.org/t/p/w780\(path)"
            if let url = URL(string: baseURL) {
                self.backdropPath = url
            }
        }
    }

    var favouriteImage: UIImage {
        isFavourite ? UIImage(systemName: "heart.fill")! : UIImage(systemName: "heart")!
    }
}
