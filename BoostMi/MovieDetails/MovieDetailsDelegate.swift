import Foundation

protocol MovieDetailsDelegate: class {
    func favouriteDidChange(for movie: MovieDetailsState)
}
