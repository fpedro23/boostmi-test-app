# Boostmi Test Project

## Contents

- [Setup](#setup)
- [App Architecture](#app-architecture)

## Setup

### Get the Source

1.  Clone the repository:

        $ git clone git@bitbucket.org:fpedro23/boostmi-test-app.git
        
2.  Set the API Key in `API.swift` file:
    
        struct API {
            static let key = "YOURKEY"
        }

### Run the App

1. Open the project in Xcode 11+.
1. Run the app in the Simulator with the `Boostmi` target.

### Running Tests

1. In Xcode's toolbar, select the project and a connected device or simulator.

   - The `Generic iOS Device` used for builds will not work for testing.

2. In Xcode's menu bar, select `Product > Test`.
   - Test results appear in the Debug Area, which can be accessed from `View > Debug Area > Show Debug Area` if not already visible.

## App Architecture

The app uses a simple unidirectional data flow architecture based on **Coordinators** and **Processors**. The following diagram provides a quick visual guide to the pattern and the way that data flows through it.
![Coordinator-Processor Pattern Diagram](https://user-images.githubusercontent.com/143393/32280552-aa4b23e2-bef2-11e7-929e-52426feb1612.png)

### Coordinators

Coordinators create Processors and Presenter views. In general, **there should be one Coordinator per `UINavigationController` in the application.**

Coordinators are based on the [Coordinator Pattern](https://will.townsend.io/2016/an-ios-coordinator-pattern) as described by Soroush Khanlou in his articles, [The Coordinator](http://khanlou.com/2015/01/the-coordinator/) and [Coordinators Redux](http://khanlou.com/2015/10/coordinators-redux/). In a nutshell, Coordinators allow us to “use UIKit as a framework inside our app, not put our app inside UIKit.”

Coordinators manage the application’s navigation stack, and leave state concerns to the Processors.

Any state that a Coordinator method requires needs to be supplied as a method parameter by the calling Processor.

**Coordinators can instantiate multiple Processors and Presenter views.**

### Processors

Processors are a variation on the Coordinator pattern, and assume responsibility for application state. **Processors are the only place where State mutation occurs.**

Whenever a Processor updates the application state, it automatically forwards the new state to its `Presenter`, which updates the UI accordingly.

If a change in state necessitates a change to the navigation stack, a Processor requests its Coordinator to make the necessary update.

Processors never make UI changes directly. This allows them to remain focused on State, and keeps things easily testable.

### Presenters

Presenters are views that visually `present` a representation of a piece of application state. A Presenter is created by a Coordinator, and assigned to a Processor.

Any time a Processor changes its piece of application state, it calls `presenter?.present(state)` so that the UI can immediately change to reflect the latest State.

### Views never mutate the application state

**Views never mutate the application state.** Instead they are assigned a Dispatcher, through which they emit **Actions**. These Actions are simple tokens of intent, and have no direct effect upon application state. Processors receive inbound Actions, and make any appropriate State changes. The changes in turn result in updates to the UI.

### Ownership

A `Coordinator` has a **strong** reference to a `UINavigationController`. It creates Processors and Presenters, but does not keep references to them. Occasionally a Coordinator has child Coordinators. In this case it needs to keep strong references to them, to prevent their deallocation.

A `Processor` has **weak** references to its `Coordinator` and `Presenter`, and a **strong** reference to its `Dispatcher`. The `State` is stored as a value type.

A `Presenter` has a **strong** reference to its `Dispatcher`, through which it can emit actions. It has no knowledge or reference to any Coordinators or Processors.


